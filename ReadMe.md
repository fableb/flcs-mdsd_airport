# MDSD Las Américas International Airport Scenery

This is Miami MDSD airport scenery for FlightGear Flight Simulator.

## Installation

- Download the repository .zip file with the link '...' -> 'Download repository'
- Extract the downloaded file to a directory of your choice
- Add this directory to 'Add-ons' -> 'Additional scenery folders' in FlightGear GUI launcher.

## Screenshots

![...](./media/Screenshot_20210130_032536.png)
![...](./media/Screenshot_20210130_032651.png)
![...](./media/Screenshot_20210130_032730.png)
![...](./media/Screenshot_20210130_033542.png)
![...](./media/Screenshot_20210130_033653.png)
![...](./media/Screenshot_20210130_033837.png)
![...](./media/Screenshot_20210130_034712.png)
![...](./media/Screenshot_20210130_035548.png)
![...](./media/Screenshot_20210130_035649.png)
![...](./media/Screenshot_20210130_035827.png)

## List of Airports I am Working On:

- MDPP - Gregorio Luperón International Airport, Puerto Plata Airport: https://bitbucket.org/fableb/flcs-mdpp_airport/src/master/
- MDSD - Las Américas International Airport: https://bitbucket.org/fableb/flcs-mdsd_airport/src/master/
- KMIA - Miami International Airport: https://bitbucket.org/fableb/flcs-kmia_airport/src/master/
- KWER - Newark Liberty International Airport: https://bitbucket.org/fableb/flcs-kewr_airport/src/master/
- TFFR - Pointe-à-Pitre International Airport: https://bitbucket.org/fableb/flcs-tffr_airport/src/master/
